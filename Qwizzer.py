import qcm
import random
import sys

###ESPACE FONCTIONS - DEBUT###:
def calc_score_end(count_score, count_score_1, count_score_2, count_score_3, sep, sep_global, feedback_lst, NAME):
    print(sep_global, "\nVoici votre score selon le système de cotation n°{0}".format(cot_mode))
    print(sep)
    print("\tVous avez fait un score de {0} point(s) sur les {1} questions de ce QCM.".format(count_score, len(feedback_lst)))
    if count_score <= len(feedback_lst)/(3):
        print("\t\tVous devriez mieux revoir la matière {0}.".format(NAME))
    elif count_score > len(feedback_lst)/(3) and count_score < 2*(len(feedback_lst))/(3):
        print("\t\tVous ferez mieux la fois prochaine {0}.".format(NAME))
    elif count_score == len(feedback_lst)/(2):
        print("\t\tPile la moitié, vous pouvez faire mieux {0}.".format(NAME))
    elif count_score >= 2*(len(feedback_lst))/(3) and count_score < len(feedback_lst):
        print("\t\tBien {0}".format(NAME))
    elif count_score == len(feedback_lst):
        print("\t\tExcellent {0}! Vous maîtrisez la matière à la perfection.".format(NAME))
    print()
    
def create_settings_file():
    #Crée le fichier settings.conf si il n'existe pas
    with open("settings.conf", "w") as file_w:
        file_w.writelines(['qcm_filename = QCM.txt\n', 'show_correction = True\n', 'admin_commands = False\n'])

def create_settings_file_admin():
    #Crée le fichier settings.conf si il n'existe pas
    with open("settings.conf", "w") as file_w:
        file_w.writelines(['qcm_filename = QCM.txt\n', 'show_correction = True\n', 'admin_commands = True\n'])
        
def read_settings_file():
    with open("settings.conf", "r") as file_r:
        return file_r.read()
    
def read_all_settings_file():
    with open("settings.conf", "r") as file_r:
        return file_r.readlines()
        
def fct_afficher_correction():
    with open("settings.conf", "r") as file_r:
        l = file_r.readlines()
        #Afficher ou non la correction (voir fichier settings)
        afficher_correction = l[1].split()
        if afficher_correction[2] == "False":
            afficher_correction = False
        else:
            afficher_correction = True
        return afficher_correction
    
def fct_filename():
    with open("settings.conf", "r") as file_r:
        l = file_r.readlines()
        #Choisir le ficher QCM à charger (voir fichier settings)
        filename = l[0].split()
        filename = filename[2]
        return filename
    
def fct_admin_commands():
    with open("settings.conf", "r") as file_r:
        l = file_r.readlines()
        #Afficher ou non les commandes admins (voir fichier settings)
        admin_commands = l[2].split()
        if admin_commands[2] == "True" or admin_commands[2] == "true" or admin_commands[2] == "TRUE":
            admin_commands = True
        else:
            admin_commands = False
        return admin_commands
    
###SPACE FONCTIONS - FIN###:
if __name__ == '__main__':
    try:
        filename = fct_filename()
    except:
        create_settings_file()
        filename = "QCM.txt"
        
        
    sep = "-"*100
    sep_global = "_"*100
    admin_commands = fct_admin_commands()
    
    #Message de Bienvenue
    if admin_commands == True:
        print("Bienvenue sur QWIZZER [MODE ADMINISTRATEUR]")
    else:
        print("Bienvenue sur QWIZZER")
    print(sep)
    print(sep)
    
    #Choisir son Prénom
    NAME = input("Veuillez entrer votre prénom: ")
    try:
        NAME = NAME.split("-")
        if len(NAME) == 2:
            NAME = NAME[0][0].upper() + NAME[0][1:] + "-" + NAME[1][0].upper() + NAME[1][1:]
        else:
            NAME = NAME[0][0].upper() + NAME[0][1:]
    except:
        NAME = ""
    print(sep)
    print("\tBienvenue {0}".format(NAME))
    print(sep)
    print(sep_global)
        
    #Commandes ADMINS
    if admin_commands == True:
    # Mise en place des quatres modes de cotations en ADMIN:
        print("!!! Programme en mode administrateur !!!".upper())
        print(sep)
        cot_mode = "temp"
        count_score = 0
        count_score_1 = 0
        count_score_2 = 0
        count_score_3 = 0
        while cot_mode != int(1) and cot_mode != int(2) and cot_mode != int(3) and cot_mode != int(4):
            cot_mode = input("""(Entrez 'help' pour plus d'informations sur les modes de cotations)
(Entrez 'user' pour savoir comment revenir aux commandes utilisatrices)
(Entrez 'qcm' pour choisir le fichier QCM à charger)
(Entrez 'correction' pour afficher ou non la correction du QCM)
(Entrez 'reset' pour recréer un fichier 'settings.conf' par défaut [À utiliser en cas de problème])
(Entrez 'exit' pour fermer le programme)\n
Choisissez votre mode de cotation (voir la section 'help') et appuyez sur 'Enter': """)
            print(sep)
            if cot_mode == "help":
                print("""Il y a quatres modes de cotations:\n\t1) Bonne réponse: +1,\n\t   Mauvaise réponse: 0,\n\t   Pas de réponse: 0\n
    \t2) Bonne réponse: +1,\n\t   Mauvaise réponse: -1,\n\t   Pas de réponse: 0\n
    \t3) Bonne réponse: +1,\n\t   Mauvaise réponse: -[Le nombre de réponses vraies] x 1) / [Le nombre de réponses fausses],\n\t   Pas de réponse: 0\n
    \t4) Combine les trois précédents systèmes de cotations""")
                print("\n[Pour ne pas répondre à une question, appuyez sur la touche 'Enter']\n[Choisissez votre mode de cotation pour passer aux questions]")
                print(sep)
            elif cot_mode == "exit":
                print(input("Aurevoir {0}, appuyez sur 'Enter' pour quitter...".format(NAME)))
                sys.exit(0)
            elif cot_mode == "user":
                print("Pour revenir aux commandes utilisatrices:\n\t1) Ouvrez le fichier 'settings.conf' avec un éditeur de texte\n\t2) Modifiez, à la ligne 3, la commande 'admin_commands = True' en 'admin_commands = False'")
                print(sep)
            elif cot_mode == "reset":
                create_settings_file_admin()
                print(input("\tFichier 'settings.conf' réinitialisé\n\tAppuiez sur 'Enter' pour continuer..."))
                print(sep)
            elif cot_mode == "qcm":
                try: 
                    data = read_settings_file()
                    last_filename = data.strip().split()
                    last_filename = last_filename[2]
                    new_filename = input("Choissisez le nom d'un fichier QCM EXISTANT en .txt\n(Si le fichier est par exemple 'QCM.txt', entrez 'QCM'): ") + ".txt"
                    print(sep)
                    with open("settings.conf", "w") as file:
                        data = data.replace("qcm_filename = {}\n".format(last_filename), "qcm_filename = {}\n".format(new_filename))
                        file.write(data)
                        print(input("\tLe fichier QCM '{0}' à été remplacé par le fichier '{1}'\n\tAppuiez sur 'Enter' pour continuer...".format(last_filename, new_filename)))
                        print(sep)
                except:
                    print("\tLa modification du fichier a échoué...\n\tEntrez la commande 'reset' puis réessayez")
                    print(sep)
                    
            elif cot_mode == "correction":
                try: 
                    data = read_settings_file()
                    l = read_all_settings_file()
                    l = l[1].split()
                    with open("settings.conf", "w") as file:
                        if "True" in l[2]:
                            data = data.replace("show_correction = True\n", "show_correction = False\n")
                            file.write(data)
                            print(input("\tLa correction au QCM N'est PLUS VISIBLE\n\tAppuiez sur 'Enter' pour continuer..."))
                            print(sep)
                        else:
                            data = data.replace("show_correction = False\n", "show_correction = True\n")
                            file.write(data)
                            print(input("\tLa correction au QCM est VISIBLE\n\tAppuiez sur 'Enter' Pour continuer..."))
                            print(sep)
                except:
                    print("\tLa modification du fichier a échoué...\n\tEntrez la commande 'reset' puis réessayez")
                    print(sep)
            else:
                try:
                    cot_mode = int(cot_mode)
                    if cot_mode != int(1) and cot_mode != int(2) and cot_mode != int(3) and cot_mode != int(4):
                        print("\tERREUR => Vous avez entré '{0}' à la place de '1', '2', '3', '4', 'help', 'correction', 'qcm', 'reset' ou 'exit'".format(cot_mode))
                        print(sep)
                except:
                    print("\tERREUR => Vous avez entré '{0}' à la place de '1', '2', '3', '4', 'help', 'user', 'correction', 'qcm', 'reset' ou 'exit'".format(cot_mode))
                    print(sep)
          
    #Commandes Utilisatrices
    else:
        # Mise en place des quatres modes de cotations:
        cot_mode = "temp"
        count_score = 0
        count_score_1 = 0
        count_score_2 = 0
        count_score_3 = 0
        while cot_mode != int(1) and cot_mode != int(2) and cot_mode != int(3) and cot_mode != int(4):
            cot_mode = input("""(Entrez 'help' pour plus d'informations sur les modes de cotations)
(Entrez 'admin' pour savoir comment accéder aux commandes administratrices)
(Entrez 'exit' pour fermer le programme)\n
Choisissez votre mode de cotation (voir la section 'help') et appuyez sur 'Enter': """)
            print(sep)
            if cot_mode == "help":
                print("""Il y a quatres modes de cotations:\n\t1) Bonne réponse: +1,\n\t   Mauvaise réponse: 0,\n\t   Pas de réponse: 0\n
    \t2) Bonne réponse: +1,\n\t   Mauvaise réponse: -1,\n\t   Pas de réponse: 0\n
    \t3) Bonne réponse: +1,\n\t   Mauvaise réponse: -[Le nombre de réponses vraies] x 1) / [Le nombre de réponses fausses],\n\t   Pas de réponse: 0\n
    \t4) Combine les trois précédents systèmes de cotations""")
                print("\n[Pour ne pas répondre à une question, appuyez sur la touche 'Enter']\n[Choisissez votre mode de cotation pour passer aux questions]")
                print(sep)
            elif cot_mode == "exit":
                print(input("Aurevoir {0}, appuyez sur 'Enter' pour quitter...".format(NAME)))
                sys.exit(0)
            elif cot_mode == "admin":
                print("Pour accéder aux commandes administratrices:\n\t1) Ouvrez le fichier 'settings.conf' avec un éditeur de texte\n\t2) Modifiez, à la ligne 3, la commande 'admin_commands = False' en 'admin_commands = True'")
                print(sep)
            else:
                try:
                    cot_mode = int(cot_mode)
                    if cot_mode != int(1) and cot_mode != int(2) and cot_mode != int(3) and cot_mode != int(4):
                        print("\tERREUR => Vous avez entré '{0}' à la place de '1', '2', '3', '4', 'help', 'admin' ou 'exit'".format(cot_mode))
                        print(sep)
                except:
                    print("\tERREUR => Vous avez entré '{0}' à la place de '1', '2', '3', '4', 'help', 'admin' ou 'exit'".format(cot_mode))
                    print(sep)
                
    # Chargement du questionnaire:
    try:
        filename = fct_filename()
    except:
        create_settings_file()
        filename = "QCM.txt"
        
    questions = qcm.build_questionnaire(filename)
    
    # Affichage de la question:
    feedback_lst = []
    print(sep_global, "\n\tLe QCM va commencer...\n\t\tBonne Chance {0} !!".format(NAME))
    print(sep)
    for q in range(len(questions)):
        num_sol = []
        print("\tQuestion " + str(q+1) + ": \"" + questions[q][0] + "\"")
        
        # Mise aléatoire des réponses:
        random_sol = questions[q][1][:]
        random.shuffle(random_sol)
        
        # Affichage des réponses:
        for r in range(len(questions[q][1])):
            num_sol.append([str(r+1), random_sol[r]])
            print("\t\t\tRéponse " + str(num_sol[r][0]) + ": \"" + num_sol[r][1][0] + "\"")
            
        sol = input("\n\t=> Saisissez le numéro de la réponse correcte selon vous et appuyez sur 'Enter': ")
        print(sep)
        
        #Ajout d'une nouvelle liste pour l'afficher dans la correction:
        try:
            feedback_lst.append([questions[q][0], num_sol[int(sol)-1][1]])
        except:
            feedback_lst.append([questions[q][0], ["Pas de Réponse", "", ""]])
        
        
        #Comptage des scores selon les quatres modes de cotations:
        #cot_mode == 1:
        try:
            if num_sol[int(sol)-1][1][1] == True:
                count_score_1 += 1
        except:
            count_score_1 += 0
            
        #cot_mode == 2:
        try:
            if num_sol[int(sol)-1][1][1] == True:
                count_score_2 += 1
            elif num_sol[int(sol)-1][1][1] == False:
                count_score_2 -= 1
        except:
            count_score_2 += 0
            
        #cot_mode == 3:
        try:
            if num_sol[int(sol)-1][1][1] == True:
                count_score_3 += 1
            elif num_sol[int(sol)-1][1][1] == False:
                nbr_true_sol = 0
                nbr_false_sol = 0
                for u in range(len(num_sol)):
                    if num_sol[u][1][1] == True:
                        nbr_true_sol += 1
                    else:
                        nbr_false_sol += 1
                count_score_3 += -(nbr_true_sol*1)/(nbr_false_sol)
        except:
            count_score_3 += 0
            
        count_score_3 = float("{0:.1f}".format(count_score_3))
                
    if cot_mode == 1:
        count_score = count_score_1
    elif cot_mode == 2:
        count_score = count_score_2
    elif cot_mode == 3:
        count_score = count_score_3
        

    #Correction des questions:
    afficher_correction = fct_afficher_correction()
    if afficher_correction == True:
        print(sep_global)
        print("Voici la correction du QCM: ".format(cot_mode))
        print(sep)
        for q in range(len(feedback_lst)):
            print("\tQuestion " + str(q+1) + ": \"" + feedback_lst[q][0] + "\"")
            print("\t\t\t- Réponse choisie : \"" + feedback_lst[q][1][0] + "\"")
            if feedback_lst[q][1][1] == True:
                print("\t\t\t- Correction: \"" + "Vous avez choisis la BONNE réponse" + "\"")
            elif feedback_lst[q][1][1] == False:
                print("\t\t\t- Correction: \"" + "Vous avez choisis la MAUVAISE réponse" + "\"")
            else:
                print("\t\t\t- Correction: \"" + "Vous N'avez PAS choisis de réponse" + "\"")
            if feedback_lst[q][1][2] != "":
                print("\t\t\t- Feedback: \"" + feedback_lst[q][1][2] + "\"")
            else:
                pass
            print(input("\n\t[Appuyez sur 'Enter' pour continuer...]"))
            print(sep)

    #Affichage du score:
    if cot_mode != 4:
        calc_score_end(count_score, count_score_1, count_score_2, count_score_3, sep, sep_global, feedback_lst, NAME)
    else:
        cot_mode = 0
        for count_score in (count_score_1, count_score_2, count_score_3):
            cot_mode += 1
            calc_score_end(count_score, count_score_1, count_score_2, count_score_3, sep, sep_global, feedback_lst, NAME)
           
    #Aurevoir
    print(sep)
    print(sep_global)
    print(input("Aurevoir {0}, appuyez sur 'Enter' pour quitter...".format(NAME)))
    sys.exit(0)
