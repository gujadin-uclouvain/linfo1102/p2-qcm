import qcm
import random
import sys

###ESPACE FONCTIONS - DEBUT###:
def calc_score_end(count_score, count_score_1, count_score_2, count_score_3, sep, sep_global, feedback_lst, NAME):
    print(sep_global, "\nVoici votre score selon le système de cotation n°{0}".format(cot_mode))
    print(sep)
    print("\tVous avez fait un score de {0} point(s) sur les {1} questions de ce QCM.".format(count_score, len(feedback_lst)))
    if count_score <= len(feedback_lst)/(3):
        print("\t\tVous devriez mieux revoir la matière, {0}.".format(NAME))
    elif count_score > len(feedback_lst)/(3) and count_score < 2*(len(feedback_lst))/(3):
        print("\t\tVous ferez mieux la fois prochaine, {0}.".format(NAME))
    elif count_score == len(feedback_lst)/(2):
        print("\t\tPile la moitié, vous pouvez faire mieux {0}.".format(NAME))
    elif count_score >= 2*(len(feedback_lst))/(3) and count_score < len(feedback_lst):
        print("\t\tBien {0}".format(NAME))
    elif count_score == len(feedback_lst):
        print("\t\tExcellent {0}! Vous maîtrisez la matière à la perfection.".format(NAME))
    print()
###SPACE FONCTIONS - FIN###:
        
if __name__ == '__main__':
    filename = "QCM.txt"
    sep = "-"*100
    sep_global = "_"*100

    # Chargement du questionnaire:
    questions = qcm.build_questionnaire(filename)
    
    #Demande du nom de l'utilisateur:
    NAME = input("Veuillez entrer votre prénom: ")
    NAME = NAME.split()
    NAME = NAME[0][0].upper() + NAME[0][1:]
    print(sep)
    print("\tBienvenue {0}".format(NAME))
    print(sep)
    print(sep_global)
    
    # Mise en place des quatres modes de cotations:
    cot_mode = "temp"
    count_score = 0
    count_score_1 = 0
    count_score_2 = 0
    count_score_3 = 0
    while cot_mode != int(1) and cot_mode != int(2) and cot_mode != int(3) and cot_mode != int(4):
        cot_mode = input("(Entrez 'help' pour plus d'informations sur les modes de cotations)\n(Entrez 'exit' pour fermer le programme)\nChoisissez votre mode de cotation (voir la section 'help') et appuyez sur 'Enter': ")
        print(sep)
        if cot_mode == "help":
            print("""Il y a quatres modes de cotations:\n\t1) Bonne réponse: +1,\n\t   Mauvaise réponse: 0,\n\t   Pas de réponse: 0\n
\t2) Bonne réponse: +1,\n\t   Mauvaise réponse: -1,\n\t   Pas de réponse: 0\n
\t3) Bonne réponse: +1,\n\t   Mauvaise réponse: -[Le nombre de réponses vraies] x 1) / [Le nombre de réponses fausses],\n\t   Pas de réponse: 0\n
\t4) Combine les trois précédents systèmes de cotations""")
            print("\n[Pour ne pas répondre à une question, appuyez sur la touche 'Enter']\n[Choisissez votre mode de cotation pour passer aux questions]")
            print(sep)
        elif cot_mode == "exit":
            sys.exit("\tAurevoir {0}".format(NAME))
        else:
            try:
                cot_mode = int(cot_mode)
                if cot_mode != int(1) and cot_mode != int(2) and cot_mode != int(3) and cot_mode != int(4):
                    print("\tERREUR => Vous avez entré '{0}' à la place de '1', '2', '3', '4', 'help' ou 'exit'".format(cot_mode))
                    print(sep)
            except:
                print("\tERREUR => Vous avez entré '{0}' à la place de '1', '2', '3', '4', 'help' ou 'exit'".format(cot_mode))
                print(sep)
                
    
    # Affichage de la question:
    feedback_lst = []
    print(sep_global, "\n\tLe QCM va commencer...\n\t\tBonne Chance {0} !!".format(NAME))
    print(sep)
    for q in range(len(questions)):
        num_sol = []
        print("\tQuestion " + str(q+1) + ": \"" + questions[q][0] + "\"")
        
        # Mise aléatoire des réponses:
        random_sol = questions[q][1][:]
        random.shuffle(random_sol)
        
        # Affichage des réponses:
        for r in range(len(questions[q][1])):
            num_sol.append([str(r+1), random_sol[r]])
            print("\t\t\tRéponse " + str(num_sol[r][0]) + ": \"" + num_sol[r][1][0] + "\"")
            
        sol = input("\n\t=> Saisissez le numéro de la réponse correcte selon vous et appuyez sur 'Enter': ")
        print(sep)
        
        #Ajout d'une nouvelle liste pour l'afficher dans la correction:
        try:
            feedback_lst.append([questions[q][0], num_sol[int(sol)-1][1]])
        except:
            feedback_lst.append([questions[q][0], ["Pas de Réponse", "", ""]])
        
        
        #Comptage des scores selon les quatres modes de cotations:
        #cot_mode == 1:
        try:
            if num_sol[int(sol)-1][1][1] == True:
                count_score_1 += 1
        except:
            count_score_1 += 0
            
        #cot_mode == 2:
        try:
            if num_sol[int(sol)-1][1][1] == True:
                count_score_2 += 1
            elif num_sol[int(sol)-1][1][1] == False:
                count_score_2 -= 1
        except:
            count_score_2 += 0
            
        #cot_mode == 3:
        try:
            if num_sol[int(sol)-1][1][1] == True:
                count_score_3 += 1
            elif num_sol[int(sol)-1][1][1] == False:
                nbr_true_sol = 0
                nbr_false_sol = 0
                for u in range(len(num_sol)):
                    if num_sol[u][1][1] == True:
                        nbr_true_sol += 1
                    else:
                        nbr_false_sol += 1
                count_score_3 += -(nbr_true_sol*1)/(nbr_false_sol)
        except:
            count_score_3 += 0
                
    if cot_mode == 1:
        count_score = count_score_1
    elif cot_mode == 2:
        count_score = count_score_2
    elif cot_mode == 3:
        count_score = count_score_3
        

#Correction des questions:
    afficher_correction = True
    if afficher_correction == True:
        print(sep_global)
        print("Voici la correction du QCM: ".format(cot_mode))
        print(sep)
        for q in range(len(feedback_lst)):
            print("\tQuestion " + str(q+1) + ": \"" + feedback_lst[q][0] + "\"")
            print("\t\t\t- Réponse choisie : \"" + feedback_lst[q][1][0] + "\"")
            if feedback_lst[q][1][1] == True:
                print("\t\t\t- Correction: \"" + "Vous avez choisis la BONNE réponse" + "\"")
            elif feedback_lst[q][1][1] == False:
                print("\t\t\t- Correction: \"" + "Vous avez choisis la MAUVAISE réponse" + "\"")
            else:
                print("\t\t\t- Correction: \"" + "Vous N'avez PAS choisis de réponse" + "\"")
            if feedback_lst[q][1][2] != "":
                print("\t\t\t- Feedback: \"" + feedback_lst[q][1][2] + "\"")
            else:
                pass
            print(input("\n\t[Appuyez sur 'Enter' pour continuer...]"))
            print(sep)

#Affichage du score:
    if cot_mode != 4:
        calc_score_end(count_score, count_score_1, count_score_2, count_score_3, sep, sep_global, feedback_lst, NAME)
    else:
        cot_mode = 0
        for count_score in (count_score_1, count_score_2, count_score_3):
            cot_mode += 1
            calc_score_end(count_score, count_score_1, count_score_2, count_score_3, sep, sep_global, feedback_lst, NAME)
